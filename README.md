# TestCaseOM


Run this project:

* open Linux terminal

* run the script:

    g++ -fPIC -Wall -g -c dynamic.cpp
    g++ -g -shared -Wl,-soname,dynamic.so.0 -o dynamic.so.0.0 dynamic.o -lc
    ldconfig -v -n .
    ln -sf dynamic.so.0 dynamic.so

    g++ -c static.cpp
    ar rc libstatic.a static.o
    ranlib libstatic.a
    
    g++ main.cpp -o main -ldynamic -L.
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
    ./main