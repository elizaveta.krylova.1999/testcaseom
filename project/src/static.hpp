#pragma once

namespace StaticLibrary {
	template<class T>
	class StaticLib {
	public:
		StaticLib() = default;

		StaticLib(T f, T s) :
			first_val(f),
			second_val(s) {};

		double sum();

	private:
		T first_val, second_val;
	};
}